#Menghubungkan dengan gspread
import csv
data1=[]
data2=[]

csv_file=open('data_user.csv','r')
csv_read=csv.reader(csv_file,delimiter=',')
data=[]
for i in csv_read :
    data1.append(i)
csv_file.close()

csv_file=open('history.csv','r')
csv_read=csv.reader(csv_file,delimiter=',')
data=[]
for i in csv_read :
    data2.append(i)
csv_file.close()

#Hangman
def garis(banyak) :
  if(0==banyak) :
    print("""——————
|
|
|
|
|
|""")
  elif(1 ==banyak) :
    print("""——————
|    |
|
|
|
|
|""")
  elif(2==banyak) :
    print("""——————
|    |
|    o
|
|
|
|""")
  elif(3==banyak) :
    print("""——————
|    |
|    o
|    |
|
|
|""")
  
  elif(4==banyak) :
    print("""——————
|    |
|    o
|    |—
|   
|
|""")
  elif(5==banyak) :
    print("""——————
|    |
|    o
|   —|—
|   
|
|""")
  elif(6==banyak) :
    print("""——————
|    |
|    o
|   —|—
|    |  
|
|""")
  elif(7==banyak) :
    print("""——————
|    |
|    o
|   —|—
|    |  
|   / 
|""")
  elif(8==banyak) :
    print("""——————
|    |
|    o
|   —|—
|    |  
|   / \\
""")

#Array berisi kata-kata tebakan

import random  
kata0=['PAYAKUMBUH','JAKARTA','KUTAI KARTANEGARA','BANDUNG','TEBING TINGGI',
'PANGKAL PINANG','TANJUNG PINANG','BANDA ACEH','BANDAR LAMPUNG','JAKARTA BARAT',
'JAKARTA SELATAN','JAKARTA TIMUR','JAKARTA UTARA','JAKARTA PUSAT','TANGERANG SELATAN'] # berisi nama nama geografis

kata1=['KUPU-KUPU','KUNANG-KUNANG','BADAK BERCULA SATU','KURA-KURA','LABA-LABA',
'LUMBA-LUMBA','BERANG-BERANG','UBUR-UBUR','BERUANG KUTUB','SAPU-SAPU','ALAP-ALAP',
'CUMI-CUMI','UNDUR-UNDUR'] # berisi nama nama hewan

kata2=['KAMBING HITAM','MEJA HIJAU','BANTING TULANG','NAIK PITAM','TANGAN KANAN',
'BESAR KEPALA','KERAS KEPALA','ANAK BUAH','ANAK EMAS','ANGKAT TANGAN','ANGKAT BICARA',
'KUTU BUKU','MUKA TEBAL','MUKA TEMBOK','MUKA DUA','PATAH HATI','PANJANG TANGAN',
'GULUNG TIKAR','BERAT HATI','BUAH TANGAN','RENDAH HATI','BUAH HATI'] # berisi nama nama ungkapan

kata3=['HALIM PERDANAKUSUMA','SULTAN ISKANDAR MUDA','SOEKARNO-HATTA','HUSEIN SASTRANEGARA',
'NGURAH RAI','AHMAD YANI','SULTAN HASANUDDIN','HUSEIN SASTRANEGARA','SYAMSUDDIN NOOR',
'SULTAN THAHA','SAM RATULANGI'] # berisi nama nama bandara

kata4=['TOLONG MENOLONG','BAHU MEMBAHU','TARIK MENARIK','PUKUL MEMUKUL','MONDAR-MANDIR',
'GOTONG-ROYONG','TEKA-TEKI','LAKI-LAKI','ANAK-ANAK','SAYUR-MAYUR','BUAH-BUAHAN','PURA-PURA'] # berisi nama nama ulangan

kata5=['FIKRI','ABYAN','JESSICA','SATRIA','NAJMI','ADITYA','NOVAL','IWAN','JERICHO',
'HILMI','FAJAR','FELISHA','FRENDY','ARSA','SOFYAN','KINANTI','ISNAINI','RIFDI','ROMZI',
'HAFIZH','AULIA','DESNANDA','CHRISTINA','RAYHAN','ARDHAN','RAFLI','IBRAHIM','SEBASTIAN',
'VIERI','CEAVIN','RYAAS','JAZMY','JOLI','FARREL','IVANA','FARHAN','THAQIF','DILLON',
'IVAN','JOHAN','ARIF','SERENA','ARYA','ARRIFQI','DIMAS','RAIHAN','WISNU'] # berisi nama teman satu kelas

#Mengecek huruf tebakan dan menghitung skor
def acak(kata) :
  gagal=1
  global score
  score=0
  keluaran=[]
  keluaran.clear()
  keluaran=['_' for i in range(len(kata))]
  for i in range(len(kata)) :
    if(kata[i]<'A') :
      keluaran[i]=kata[i]
    else :
      keluaran[i]="_"
  #keluaran=['_' for i in range(len(kata))] 
  while(gagal!=0) :
    salah=0 # buat mengecek huruf ada yang benar atau tidak
    garis(score)
    
    print()
    for ii in range(len(kata)) : # buat nampilin huruf yang telah ditebak
      print(keluaran[ii],end=' ')
    print()
    print()
    gagal=0

    pil=input("Masukkan huruf : ")

    for jj in range(len(kata)) : # mengecek huruf yang ada didalam kata
      if(kata[jj]==pil) :
        keluaran[jj]=pil
        salah=1
      
    
    for a in range(len(kata)) : # mengecek program telah selesai atau belum
      if(keluaran[a]!=kata[a]) :
        gagal=1

    if(salah==0) : # untuk menambah index score apabila ada kesalahan
      print(f"Tebakan huruf {pil} salah")
      score=score+1
      
    #untuk menampilkan score 
    if(score==8) : # kalau score 8 gagal=0 artinya program telah selesai 
      gagal=0
      print("Skor anda sekarang 0")
    else :
      print("Skor anda sekarang",100-score*score)
    
    
  garis(score)
  for ii in range(len(kata)) : # buat nampilin huruf yang telah ditebak
    print(keluaran[ii],end=' ')
  print() 
#Login/Register
print("Selamat datang di permainan Tebak Kata!")
cekk=0
class text:
  BOLD = '\033[1m'
  UNDERLINE = '\033[4m'
  END = '\033[0m'

print("Silahkan login :")
user = input("Username: ")
for i in range(1,len(data1)) :
  if(data1[i][0]==user) :
    nodata=i
    cekk=1
if (cekk==1):
  print("Selamat datang kembali,", user, "!")
else:
  print("Anda belum terdaftar. Silahkan daftarkan diri Anda")
  regname = input("Nama: ")
  regzod = input("Zodiak: ")
  print("Selamat datang,", user, "! Kamu telah terdaftar.")
  
  data2.append([user,0])
  data1.append([user,regname,regzod,0])
  nodata=len(data1)-1

  ss='data_user.csv'
  with open(ss,'w',newline='\n') as file :
      writer = csv.writer(file,delimiter=',')
      writer.writerows(data1)
  
  ss='history.csv'
  with open(ss,'w',newline='\n') as file :
      writer = csv.writer(file,delimiter=',')
      writer.writerows(data2)
  

  
cekk=0

#Menu
def game():
  global regname
  global user
  global nodata
  global selesai
  global nodata2
  print("""Pilih Game Menu:
1. Change User
2. Top 10 Tebak Kata
3. Start Game 
4. Info pengguna
5. Keluar
""")


  b = int(input("Pilih Menu(1-5): "))
  #Change User
  if (b == 1):
    cekk=0                      # data belum tersimpan di gsheets
    user = input("Username: ")
    for i in range(1,len(data1)) :    #len(data1) adalah kolom terakhir yang digunakan di sheets
      if(data1[i][0]==user) :    
        nodata=i                      #buat nandain kolom ke berapa                 
        cekk=1
    if (cekk==1):
      print("Selamat datang kembali,", user, "!")
    else:
      print("Anda belum terdaftar. Silahkan daftarkan diri Anda")
      regname = input("Nama: ")
      regzod = input("Zodiak: ")
      print("Selamat datang,", user, "! Kamu telah terdaftar.")

      

      data1.append([user,regname,regzod,int(0)])
      data2.append([user,int(0)])
      nodata=len(data1)-1
      ss='data_user.csv'
      with open(ss,'w',newline='\n') as file :
          writer = csv.writer(file,delimiter=',')
          writer.writerows(data1)
      
      ss='history.csv'
      with open(ss,'w',newline='\n') as file :
          writer = csv.writer(file,delimiter=',')
          writer.writerows(data2)

  #Algoritma sorting (untuk mengurutkan data username&skor)   
  elif (b == 2):
    data1s=[]
    csv_file=open('data_user.csv','r')
    csv_read=csv.reader(csv_file,delimiter=',')
    
    for i in csv_read :
        data1s.append(i)
    csv_file.close()
                
    
    for i in range(1,len(data1s),1) :              
      for j in range(i+1,len(data1s),1) :
        if(int(data1s[i][3])<int(data1s[j][3])) :
          temp0=data1s[i][0]      
          temp1=data1s[i][1]
          temp2=data1s[i][2]
          temp3=data1s[i][3]

          data1s[i][0]=data1s[j][0]
          data1s[i][1]=data1s[j][1]
          data1s[i][2]=data1s[j][2]
          data1s[i][3]=data1s[j][3]

          data1s[j][0]=temp0
          data1s[j][1]=temp1
          data1s[j][2]=temp2
          data1s[j][3]=temp3

        
    for i in range(1,len(data1s),1) :       
      for j in range(1,len(data1s),1) : 
        if(data1s[i][3]==data1s[j][3]) :
          if(data1s[i][0]<data1s[j][0]) :
            temp0=data1s[i][0]
            temp1=data1s[i][1]
            temp2=data1s[i][2]
            temp3=data1s[i][3]

            data1s[i][0]=data1s[j][0]
            data1s[i][1]=data1s[j][1]
            data1s[i][2]=data1s[j][2]
            data1s[i][3]=data1s[j][3]

            data1s[j][0]=temp0
            data1s[j][1]=temp1
            data1s[j][2]=temp2
            data1s[j][3]=temp3
          
 #Tabel Top 10    
    for line in range(1,min(len(data1s)-1,10)+1):   
      
      if line == 1 :
        print()
        print(text.BOLD + (f"TOP {min(len(data1s)-1,10)} TEBAK KATA".center(60)) + text.END)
        print()
        print("NO.".center(20), "USERNAME".center(20), "SCORE".center(20))
        print("———————————————" * 4)

      print(str(line).center(20), end=" ")
      print(str(data1s[line][0]).center(20), end=" ")
      print(str(data1s[line][3]).center(20), end=" ")
      
      print("")
      print("———————————————" * 4)

  #Program inti game
  elif (b == 3):
    print("""Pilih kategori yang Anda inginkan :
1. Kota
2. Hewan
3. Ungkapan
4. Bandara
5. Kata ulang
6. Nama-nama temen sekelas""")
    pil=int(input("Pilih kategori(1-6) : "))
    kata0s=kata0.copy()     
    kata1s=kata1.copy()
    kata2s=kata2.copy()
    kata3s=kata3.copy()
    kata4s=kata4.copy()
    kata5s=kata5.copy()
    #print(data2[nodata])
    #print('len',len(data2[nodata]))
    for i in range(2,len(data2[nodata])) : #menandakan kata yang sudah pernah ditebak 
      katas=data2[nodata][i]
      #print(katas)
      for j in range(len(kata0s)) : 
        if(kata0s[j]==katas) :
          kata0s.pop(j)
          break 
        
      for j in range(len(kata1s)) : 
        if(kata1s[j]==katas) :
          kata1s.pop(j)
          break 
        
      for j in range(len(kata2s)) :   
        if(kata2s[j]==katas) :
          kata2s.pop(j)
          break 
        
      for j in range(len(kata3s)) :
        if(kata3s[j]==katas) :
          kata3s.pop(j)
          break

      for j in range(len(kata4s)) :
        if(kata4s[j]==katas) :
          kata4s.pop(j)
          break

      for j in range(len(kata5s)) :
        if(kata5s[j]==katas) :
          kata5s.pop(j)
          break

    data2[nodata][1]=int(data2[nodata][1])+1  
    ss='history.csv'
    with open(ss,'w',newline='\n') as file :
        writer = csv.writer(file,delimiter=',')
        writer.writerows(data2)
    

    if(pil==1) :
      huruf=random.choice(kata0s)
      acak(huruf)
        
    elif(pil==2) :
      huruf=random.choice(kata1s)
      acak(huruf)
        
    elif(pil==3) :
      huruf=random.choice(kata2s)
      acak(huruf)
        
    elif(pil==4) :
      huruf=random.choice(kata3s)
      acak(huruf)
        
    elif(pil==5) :
      huruf=random.choice(kata4s)
      acak(huruf)
        
    elif(pil==6) :
      huruf=random.choice(kata5s)
      acak(huruf)
        
      
      
    data2[nodata].append(huruf)
    ss='history.csv'
    with open(ss,'w',newline='\n') as file :
        writer = csv.writer(file,delimiter=',')
        writer.writerows(data2)
        
    nilai=100-score*score
    if(score==8) :
      nilai=0
    print(f"Nilai anda sekarang yaitu {nilai}")
    inp=input(("Apakah anda mau memasukkan nilai anda? (yes/no) "))
    data1[nodata][3]=nilai
    if(inp=='yes') :
      data1[nodata][3]==nilai
      ss='history.csv'
      with open(ss,'w',newline='\n') as file :
          writer = csv.writer(file,delimiter=',')
          writer.writerows(data1)
       
  

    
  #Info Pengguna :
  elif (b == 4):
    print(text.BOLD + ("INFO PENGGUNA".center(60)) + text.END)
    print()
    print('Username',end='\t')
    print(":", data1[nodata][0])
    print('Nama',end='\t\t')
    print(":", data1[nodata][1])
    print('Zodiak',end='\t\t')
    print(":", data1[nodata][2])
    print('Score',end='\t\t')
    print(":", data1[nodata][3])
    print('Banyak permainan',end='')
    print(":",data2[nodata][1])

#Keluar game  
  elif (b == 5):
    selesai = True
    print("Apakah anda yakin akan keluar?")
  else:
    print("Input tidak sesuai.")


selesai = False

while (selesai == False):
  #print(data1)
  game()
  print()
  
  choice = input("Kembali ke Game Menu? (yes/no): ")
  if (choice == "no" or selesai==True):
    selesai = True
    print("Sampai jumpa lagi",user,"T_T!")
  else:
    selesai = False
